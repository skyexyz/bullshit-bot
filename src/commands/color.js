const { ROLE_ID } = process.env

const Color = (logger, {  }) => {
    this.logger = logger

    return {
        name: 'RunNow',
        execute: async ({ message }) => {
            const args = message.content.split(/\s/gi)
            if (args.length < 2) {
                message.channel.send('Please provide a colour')
                return
            }
            const color = args[1]

            if (/^#[0-9A-F]{6}$/i.test(color)) {
                const role = message.guild.roles.cache.find((r) => r.id === ROLE_ID)
                await role.edit({
                    color: color,
                })
                message.react('✅')
            } else {
                message.channel.send('Provided colour is invalid')
            }
        },
        test: async (message) =>
            message.content.startsWith('!color') ||
            message.content.startsWith('!colour'),
    }
}

module.exports = Color
