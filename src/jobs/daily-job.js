const { GUILD_ID, ROLE_ID, OPTOUT_ROLE_ID, CHANNEL_ID, CAST_TOKEN, CAST_URI } = process.env
const fetch = require('node-fetch')
const crypto = require("crypto")
const { Guild, Client } = require('discord.js')

const colors = [
    '#8585ff',
    '#fff681',
    '#a073fd',
    '#fd73b9',
    '#DA1884',
    '#420BEF',
    '#696969',
    '#B00B69',
    '#FF5891',
    '#ff0000',
    '#68ddb5',
]

const plinko = [
    `you have to add a signature to all of your posts like an old forum`,
    `only post images/gifs/emotes/videos/stickers`,
    `gain nickname changing perms`,
    `roleplay as another poster`,
    `every message has to include a COMPLIMENT`,
    `every message has to include a ROAST 🔥 `,
    `only speak like a poet`,
    `precede each message with "L + ratio + "`,
    `add a custom server rule of your choice we all have to follow for the next week`,
    `every message needs a completely unrelated emote`,
    `you have to spell every word wrong`,
    `all messages have to be in full caps`,
    `get to pick a new server image`,
]

const removeRoleFromEveryone = async (membersArr, role) => {
    for (let i = 0; i < membersArr.length; i++) {
        const m = membersArr[i]
        await m.roles.remove(role)
    }
}

const rigged = async (membersArr, role, userId) => {
    await removeRoleFromEveryone(membersArr, role)
    await membersArr.find((x) => x.user.id === userId).roles.add(role)
}

const getNewMember = (membersArr, lastUsers) => {
    const id = Math.floor(Math.random() * membersArr.length)
    let member = membersArr[id]
    while (
        member.user.bot ||
        lastUsers.includes(member.user.id) ||
        member.roles.cache.has(OPTOUT_ROLE_ID)
    ) {
        const newId = Math.floor(Math.random() * membersArr.length)
        member = membersArr[newId]
    }

    return member
}

const DailyJob = (logger, Config, client) => {
    this.logger = logger
    /** @type {Client} */
    this.client = client
    this.role = null
    const self = this

    return {
        name: 'DailyJob',
        cronExpression: '0 14 * * *',
        task: async (now) => {
            this.logger.info('Running daily task')

            const config = await Config.getConfig()
            const lastUsers = await Config.getLastUsers()
            let lastUsersConf = config.lastUser

            const guild = await self.client.guilds.fetch(GUILD_ID)
            const members = (await guild.members.fetch()).array()
            if (self.role === null) {
                self.role = guild.roles.cache.find((r) => r.id === ROLE_ID)
            }
            await self.role.edit({
                color: colors[Math.floor(Math.random() * colors.length)],
            })

            this.logger.info('Handle daily mode')
            if (config.rigged) {
                this.logger.info(`It's rigged lol`)
                rigged(members, self.role, config.rigged.userId)
                config.lastUser = []
                config.lastUser.push(config.rigged.userId)
            } else {
                this.logger.info('Picking daily member')
                let member = getNewMember(members, lastUsers)
                await removeRoleFromEveryone(members, self.role)
                await member.roles.add(self.role)
                lastUsers.push(member.id)
                config.lastUser = []
                config.lastUser.push(member.user.id)
                if (Math.random() < 0.15) {
                    this.logger.info('DOUBLE USER TIME')
                    let memberTwo = getNewMember(members, lastUsers)
                    await memberTwo.roles.add(self.role)
                    config.lastUser.push(memberTwo.user.id)
                    lastUsers.push(memberTwo.user.id)
                }
            }

            const channel = await self.client.channels.fetch(CHANNEL_ID)
            this.logger.info('Sending participation message...')
            if (lastUsersConf.length !== 0) {
                const lastUser = lastUsersConf
                    .map((id) => `<@${id}>`)
                    .join(' and ')
                const message = await channel.send(
                    `Thank you for participating ${lastUser}, but your time is over. It's now time for your peers to vote on your performance.\nPlease use the buttons below.`
                )
                await message.react('👍')
                await message.react('👎')
            }
            this.logger.info('Sending congratulations message...')
            if (config.rigged) {
                await channel.send(config.rigged.message)
                config.rigged = null
            } else {
                const currentUsers = config.lastUser
                    .map((id) => `<@${id}>`)
                    .join(' and ')
                await channel.send(
                    `Congratulations ${currentUsers}, you can now post whatever you want in here for the next 24 hours!`
                )
                if (config.plinkoEnabled) {
                    if (Math.random() < 0.25) {
                        config.lastUser.forEach(async (id) => {
                            const pick =
                                plinko[Math.floor(Math.random() * plinko.length)]
                            await channel.send(
                                `<@${id}>: You are assigned the following item from poster plinko: ${pick}`
                            )
                        })
                    }
                }
            }
            if (CAST_TOKEN) {
                try {
                    const currentUsers = config.lastUser
                        .map((id) => `<@${id}>`)
                        .join(' and ')
                    const newPassword = crypto.randomBytes(20).toString('hex');
                    const res = await fetch(`${CAST_URI}/api/station/7/streamer/2`, {
                        method: 'PUT', body: { "streamer_password": newPassword, "display_name": `DJ ${currentUsers}` }, headers: {
                            "content-type": "application/json",
                            "authorization": `Bearer ${CAST_TOKEN}`
                        }
                    })
                    this.logger.info(JSON.stringify(res))
                    this.logger.info(`New streamer password set to ${newPassword}`)
                    config.lastUser.forEach(async id => {
                        const member = guild.member(id)
                        if (member !== null) {
                            try {
                                const dm = await member.createDM()
                                await dm.send(`The password for streaming is ${newPassword}. Have fun.`)
                            } catch (ex) {
                                this.logger.error(ex)
                            }
                        }
                    })
                } catch (ex) {
                    this.logger.error(ex)
                }
            }
            await Config.save(config, lastUsers)
        },
    }
}

module.exports = DailyJob
