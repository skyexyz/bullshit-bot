FROM node:16-alpine

ENV BOT_TOKEN="" \
  GUILD_ID="" \
  ROLE_ID="" \
  CHANNEL_ID="" \
  OPTOUT_ROLE_ID=""

WORKDIR /app

COPY src/ /app/

RUN npm ci

CMD ["npm", "run", "start"]
