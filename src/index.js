// @ts-check
if (process.env.NODE_ENV === 'development') {
    require('dotenv').config({ path: '../.env' })
}
const fs = require('fs')
const path = require('path')
const Discord = require('discord.js')
const { Intents } = require('discord.js')
// @ts-ignore
const logger = require('pino')()
const Config = require('./config')(logger.child({ class: 'Config' }))
const JobManager = require('./job-manager')(
    logger.child({ class: 'JobManager' })
)
const CommandManager = require('./command-manager')(
    logger.child({ class: 'CommandManager' })
)

const { BOT_TOKEN } = process.env

const intents = new Intents([Intents.NON_PRIVILEGED, 'GUILD_MEMBERS'])
const client = new Discord.Client({ ws: { intents } })

const addJobs = () => {
    JobManager.addJob(
        require('./jobs/daily-job')(
            logger.child({ job: 'DailyJob' }),
            Config,
            client
        )
    )
}

const addCommands = () => {
    const commandsPath = path.join(__dirname, './commands')
    const files = fs.readdirSync(commandsPath)
    files.forEach((file) => {
        CommandManager.addCommand(
            require(path.join(commandsPath, file))(
                logger.child({ command: file.split('.')[0] }),
                {
                    JobManager,
                    Config,
                }
            )
        )
    })
}

;(async () => {
    addJobs()
    addCommands()

    client.on('ready', async () => {
        logger.info(`Logged in as ${client.user.tag}!`)
        JobManager.setupJobs()
    })

    client.on('message', CommandManager.handler)

    client.login(BOT_TOKEN)
})()
