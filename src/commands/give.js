const { ROLE_ID } = process.env

const getId = (str) => {
    const matches = str.match(/<@!?(\d+)>/)
    if (matches && matches.length === 2) {
        return matches[1]
    }
    return null
}

const Give = (logger, { Config }) => {
    this.logger = logger
    this.role = null
    const self = this

    return {
        name: 'Give',
        execute: async ({ message }) => {
            const args = message.content.split(/\s/gi)
            if (args.length < 2) {
                message.channel.send('Please provide a user')
            }
            const members = args.slice(1)
            const guildMembers = (await message.guild.members.fetch()).array()

            if (self.role === null) {
                self.role = message.guild.roles.cache.find(
                    (r) => r.id === ROLE_ID
                )
            }
            if (members[0] === 'everyone') {
                guildMembers.forEach((guildMember) => {
                    guildMember.roles.add(self.role)
                })
            } else {
                for (let i = 0; i < members.length; i++) {
                    const member = members[i]
                    const id = getId(member)
                    logger.info({ member, id })
                    if (!id) {
                        continue
                    }

                    const guildMember = guildMembers.find((x) => x.id === id)
                    if (!guildMember) {
                        continue
                    }
                    guildMember.roles.add(self.role)
                }
            }
        },
        test: async (message) => {
            const config = await Config.getConfig()

            return (
                (message.guild && message.author.id === message.guild.ownerID ||
                    config.lastUser.indexOf(message.author.id) !== -1) &&
                message.content.startsWith('!give')
            )
        },
    }
}

module.exports = Give
