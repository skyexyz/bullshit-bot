const findAsync = async (arr, asyncCallback) => {
    const promises = arr.map(asyncCallback)
    const results = await Promise.all(promises)
    const index = results.findIndex((result) => result)
    return arr[index]
}

const CommandManager = (logger) => {
    this.logger = logger
    this.commands = []

    this.logger.info('CommandManager initialized')

    return {
        addCommand: (command) => {
            this.commands.push(command)
            this.logger.info(`Adding command ${command.name}`)
        },
        handler: async (message) => {
            const command = await findAsync(this.commands, async (thing) => {
                return (await thing.test(message)) === true
            })
            if (command) {
                this.logger.info(`Running command ${command.name}`)
                command.execute({ message })
            }
        },
    }
}

module.exports = CommandManager
