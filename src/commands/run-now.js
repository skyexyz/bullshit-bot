const RunNow = (logger, { JobManager }) => {
    this.logger = logger

    return {
        name: 'RunNow',
        execute: async ({ message }) => {
            await JobManager.getJob('DailyJob').task()
        },
        test: async (message) =>
            message.guild !== null && message.author.id === message.guild.ownerID &&
            message.content.startsWith('!run-now'),
    }
}

module.exports = RunNow
