const configFile = './config/config.json'
const lastUsersFile = './config/lastUsers.json'
const fs = require('node:fs')

const Config = (logger) => {
    this.logger = logger
    this.config = null
    this.lastUsers = null

    this.logger.info('Config initialized')

    return {
        getConfig: async (force) => {
            if (this.config !== null && !force) {
                return this.config
            }

            try {
                let config = JSON.parse(fs.readFileSync(configFile, 'utf-8'))

                this.config = config
                return this.config
            } catch (e) {
                return {
                    lastUser: [],
                    rigged: null,
                }
            }
        },
        getLastUsers: async (force) => {
            if (this.lastUsers !== null && !force) {
                return this.lastUsers
            }
            try {
                const lastUsers = JSON.parse(
                    fs.readFileSync(lastUsersFile, 'utf-8')
                ).slice(-5)
                this.lastUsers = lastUsers
                return this.lastUsers
            } catch (e) {
                return []
            }
        },
        save: async (config, lastUsers) => {
            if (config) {
                this.config = config
                fs.writeFileSync(configFile, JSON.stringify(config), 'utf-8')
            }
            if (lastUsers) {
                this.lastUsers = lastUsers
                fs.writeFileSync(
                    lastUsersFile,
                    JSON.stringify(lastUsers),
                    'utf-8'
                )
            }
        },
    }
}

module.exports = Config
