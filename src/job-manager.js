// @ts-check
const cron = require('node-cron')

/**
 * @callback TypeFnJobTask
 * @param {string} now
 * @returns {Promise}
 */

/**
 * @typedef {Object} Job
 * @property {TypeFnJobTask} task
 * @property {string} name
 */

/**
 * @callback TypeFnAddJob
 * @param {Job} job
 * @returns {void}
 */

/**
 * @callback TypeFnGetJob
 * @param {string} name
 * @returns {Job}
 */

/**
 * @typedef {Object} JobManager
 * @property {TypeFnAddJob} addJob
 * @property {TypeFnGetJob} getJob
 * @property {Function} setupJobs
 */

/**
 * 
 * @param {object} logger 
 * @returns {JobManager}
 */
const JobManager = (logger) => {
    this.logger = logger
    this.jobs = []

    this.logger.info('JobManager initialized')

    return {
        addJob: (job) => {
            this.logger.info(`Adding job ${job.name}`)
            this.jobs.push(job)
        },
        setupJobs: () => {
            this.logger.info(`Setting up jobs...`)
            this.jobs.forEach((job) => {
                if (cron.validate(job.cronExpression)) {
                    cron.schedule(job.cronExpression, job.task)
                    this.logger.info(
                        `Job '${job.name}' scheduled for ${job.cronExpression}`
                    )
                } else {
                    logger.info('Invalid cron expression for job', job.name)
                }
            })
        },
        getJob: (name) => {
            return this.jobs.find((x) => x.name === name)
        },
    }
}

module.exports = JobManager
