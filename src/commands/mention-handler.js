const { ROLE_ID } = process.env

const MentionHandler = (logger, { Config }) => {
    this.logger = logger
    this.role = null
    const self = this

    return {
        name: 'Give mention handler',
        execute: async ({ message }) => {
            if (self.role === null) {
                self.role = message.guild.roles.cache.find(
                    (r) => r.id === ROLE_ID
                )
            }

            const users = [...message.mentions.users.values()]
            const guildMembers = (await message.guild.members.fetch()).array()
            if (message.mentions.everyone) {
                guildMembers.forEach((guildMember) => {
                    guildMember.roles.add(self.role)
                })
            } else if (users.length > 0) {
                for (let i = 0; i < users.length; i++) {
                    const user = users[i]
                    const member = guildMembers.find((x) => x.id === user.id)
                    if (!member) {
                        continue
                    }
                    member.roles.add(self.role)
                }
            }
        },
        test: async (message) => {
            const config = await Config.getConfig()

            return (
                !message.content.startsWith('!') &&
                config.lastUser.indexOf(message.author.id) !== -1 &&
                ([...message.mentions.users.values()].length > 0 ||
                    message.mentions.everyone)
            )
        },
    }
}

module.exports = MentionHandler
